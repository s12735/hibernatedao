package DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import Model.Client;
import Model.Reservation;
import Model.Room;
import Util.HibernateUtil;

public class DAO {

	public Boolean addClient(Client client) {

		Transaction trns = null;
		Boolean validate = null;
		Session session = HibernateUtil.getSessionFactory().openSession();

		try {
			trns = session.beginTransaction();
			int clientid = (Integer) session.save(client);
			session.getTransaction().commit();
			validate = true;
		} catch (RuntimeException e) {
			validate = false;
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		return validate;
	}

	public Boolean deleteClient(Client client) {
		Transaction trns = null;
		Boolean validate = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			trns = session.beginTransaction();
			session.delete(client);
			session.getTransaction().commit();
			validate = true;
		} catch (RuntimeException e) {
			validate = false;
			if (trns != null) {
				trns.rollback();
			}
		} finally {
			session.flush();
			session.close();
		}

		return validate;
	}

	public Boolean deleteClient(int clientid) {
		Transaction trns = null;
		Boolean validate = null;
		Session session = HibernateUtil.getSessionFactory().openSession();

		try {
			trns = session.beginTransaction();
			Client client = (Client) session.load(Client.class, new Integer(clientid));
			session.delete(client);
			session.getTransaction().commit();
			validate = true;
		} catch (RuntimeException e) {
			validate = false;
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}

		return validate;
	}

	public Boolean updateClient(Client client) {
		Boolean valid = null;
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			trns = session.beginTransaction();
			session.update(client);
			session.getTransaction().commit();
			valid = true;
		} catch (RuntimeException e) {
			valid = false;
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		return valid;
	}

	public List<Client> getAllClients() {
		List<Client> clients = null;
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			trns = session.beginTransaction();
			clients = session.createQuery("from Client").list();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		return clients;
	}

	public Client getClient(Client client) {
		int clientid = client.getClientID();
		Client ur = new Client();
		ur = getClientById(clientid);
		return ur;

	}

	public Client getClientById(int clientid) {
		Client client = null;
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			trns = session.beginTransaction();
			String queryString = "from Client where id = :id";
			Query query = session.createQuery(queryString);
			query.setInteger("id", clientid);
			client = (Client) query.uniqueResult();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		return client;
	}

	public Boolean addRoom(Room room) {

		Transaction trns = null;
		Boolean validate = null;
		Session session = HibernateUtil.getSessionFactory().openSession();

		try {
			trns = session.beginTransaction();
			session.save(room);
			session.getTransaction().commit();
			validate = true;
		} catch (RuntimeException e) {
			validate = false;
			if (trns != null) {
				trns.rollback();
			}
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		return validate;
	}

	public Room getRoomByNumber(String roomNumb) {
		Room room = null;
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			trns = session.beginTransaction();
			String queryString = "from Room where roomNumber = :roomNumber";
			Query query = session.createQuery(queryString);
			query.setString("roomNumber", roomNumb);
			room = (Room) query.uniqueResult();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		return room;
	}

	public List<Room> getAllRooms() {
		Transaction trns = null;
		List<Room> rooms = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			trns = session.beginTransaction();
			rooms = session.createQuery("from Room").list();
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		return rooms;
	}

	public List<Reservation> getAllRreservations() {
		Transaction trns = null;
		List<Reservation> reservations = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			trns = session.beginTransaction();
			reservations = session.createQuery("from Reservation").list();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		return reservations;
	}
}
