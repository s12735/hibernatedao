import java.util.Date;

import DAO.DAO;
import Model.Client;
import Model.Reservation;
import Model.Reservation.ReservationType;
import Model.Room;
import Model.Setup;

public class App {

	public static void main(String[] args) {

		// Dodanie klienta
		Client u = new Client();
		u.setPhoneNumber("5930865295");
		u.setEmail("lukasz.grabowski6@gmail.com");
		new DAO().addClient(u);

		// Dodadanie rezerwacji
		Reservation r = new Reservation();
		Reservation rw = new Reservation();
		Reservation rww = new Reservation();
		r.setStartDate(new Date());
		r.setEndDate(new Date());
		r.setReservationType(ReservationType.potwierdzona);
		r.setRoomID("1");
		rw.setStartDate(new Date());
		rw.setEndDate(new Date());
		rw.setReservationType(ReservationType.potwierdzona);
		rw.setRoomID("1");
		rww.setRoomID("1");
		u.addReservation(r);
		u.addReservation(rww);

		// Usunięcie rezerwacji
		u.removeReservation(r);

		// Uzyskanie klienta na podstawie rezerwacji jest możliwe po dodaniu
		// rezerwacji do osoby
		Client client = rww.getClient();

		// Możemy dostać wszystkie rezerwacje klienta
		client.getReservations();

		for (Reservation rez : client.getReservations()) {
			System.out.println(rez);
		}
		
		// Obliczenie kosztu rezerwacji
		Room room = new DAO().getRoomByNumber(rw.getRoomID());
		int numberOfDates = 5; // ma być (rezerwacja.endDate -
								// rezerwacja.startDate) ilość dni
		System.out.println(
				"Cena noclegu: " + room.getNumberOfPersons() * room.getPricePerPerson() * numberOfDates + "zł");

		// Pobranie wszystkich osób z bazy // new DAO().getAllClients()
		for (Client cl : new DAO().getAllClients()) {
			System.out.println(cl);
		}

		// Pobranie dostępnych pokoi
		new DAO().getAllRooms();

		for (Room roo : new DAO().getAllRooms()) {
			System.out.println(roo);
		}

		// Pobranie wszystki rezerwacji
		new DAO().getAllRreservations();

		for (Reservation rew : new DAO().getAllRreservations()) {
			System.out.println(rew);
		}
		
//		Ustawienie email
//		Setup().Email(true, true, "fd", "fd", "fdf", "fdd", "fdfd");

	}
}