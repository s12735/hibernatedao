package Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ROOMS")
public class Room {

	@Id
	@Column(name = "roomNumber")
	private String roomNumber;

	@Column(name = "numberOfPersons")
	private int numberOfPersons;

	@Column(name = "pricePerPerson")
	private int pricePerPerson;

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public int getNumberOfPersons() {
		return numberOfPersons;
	}

	public void setNumberOfPersons(int numberOfPersons) {
		this.numberOfPersons = numberOfPersons;
	}

	public int getPricePerPerson() {
		return pricePerPerson;
	}

	public void setPricePerPerson(int pricePerPerson) {
		this.pricePerPerson = pricePerPerson;
	}

	@Override
	public String toString() {
		return "Room [roomNumber= " + roomNumber + ", numberOfPersons= " + numberOfPersons + ", pricePerPerson= "
				+ pricePerPerson + "]";
	}
}
