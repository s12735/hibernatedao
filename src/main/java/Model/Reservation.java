package Model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "RESERVATIONS")
public class Reservation {

	@Id
	@GeneratedValue
	@Column(name = "reservationID")
	private int reservationID;

	@Column(name = "startDate")
	private Date startDate;

	@Column(name = "endDate")
	private Date endDate;

	@Column(name = "reservationType")
	private ReservationType reservationType = ReservationType.dodana;

	@ManyToOne
	@JoinColumn(name = "clientID")
	private Client client;

	@Column(name = "roomNumber")
	private String roomNumber;

	public String getRoomID() {
		return roomNumber;
	}

	public void setRoomID(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client user) {
		this.client = user;
	}

	public enum ReservationType {
		dodana, potwierdzona, anulowana
	}

	public int getReservationID() {
		return reservationID;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public ReservationType getReservationType() {
		return this.reservationType;
	}

	public void setReservationID(int reservationID) {
		this.reservationID = reservationID;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setReservationType(ReservationType reservationType) {
		this.reservationType = reservationType;
	}

	@Override
	public String toString() {
		return "Reservation [reservationid=" + reservationID + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", room=" + roomNumber + ", reservationType=" + reservationType + ", client" + client + "]";
	}
}
