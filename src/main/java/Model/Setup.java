package Model;

import DAO.DAO;
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class Setup {
	public static void addRoom(String roomNumber, int numberOfPersons, int pricePerPerson) {
		Room room = new Room();
		room.setRoomNumber(roomNumber);
		room.setNumberOfPersons(numberOfPersons);
		room.setPricePerPerson(pricePerPerson);
		new DAO().addRoom(room);
	}
	
	public static void emailConfig(Boolean smtp_auth, Boolean smtp_starttls_enable,
			String smtp_host, String smtp_port, 
			String username, String password, String sendername){
		try {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("settings");
			doc.appendChild(rootElement);
			
			Element smtp_a = doc.createElement("smtp_auth");
			rootElement.appendChild(smtp_a);
			smtp_a.appendChild(doc.createTextNode(smtp_auth.toString()));
			
			Element smtp_starttls = doc.createElement("smtp_starttls_enable");
			rootElement.appendChild(smtp_starttls);
			smtp_starttls.appendChild(doc.createTextNode(smtp_starttls_enable.toString()));
			
			Element smtp_h = doc.createElement("smtp_host");
			rootElement.appendChild(smtp_h);
			smtp_h.appendChild(doc.createTextNode(smtp_host));
			
			Element smtp_p = doc.createElement("smtp_port");
			rootElement.appendChild(smtp_p);
			smtp_p.appendChild(doc.createTextNode(smtp_port));
			
			Element userName = doc.createElement("username");
			rootElement.appendChild(userName);
			userName.appendChild(doc.createTextNode(username));
			
			Element pass = doc.createElement("password");
			rootElement.appendChild(pass);
			pass.appendChild(doc.createTextNode(password));
			
			Element senderName = doc.createElement("senderName");
			rootElement.appendChild(senderName);
			senderName.appendChild(doc.createTextNode(sendername));
			
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("config.dat"));
			transformer.transform(source, result);

			System.out.println("File saved!");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		
		
		
	}
}
