package Model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.IndexColumn;

import DAO.DAO;
import Model.Reservation.ReservationType;
import email.Email;

@Entity
@Table(name = "CLIENTS")
public class Client {

	@Id
	@GeneratedValue
	@Column(name = "clientID")
	private int clientID;

	@Column(name = "firstName")
	private String firstName;

	@Column(name = "lastName")
	private String lastName;

	@Column(name = "phoneNumber")
	private String phoneNumber;

	@Column(name = "email")
	private String email;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	@JoinColumn(name = "clientID")
	@IndexColumn(name = "idx")
	private List<Reservation> reservations = new ArrayList<Reservation>();

	public Client() {
	}

	public Client(String firstName, String lastName, String phoneNumber, String email, List<Reservation> reservations) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.email = email;
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientid) {
		this.clientID = clientid;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public void addReservation(Reservation reservation) {
		reservation.setClient(this);
		reservations.add(reservation);
		if (new DAO().updateClient(this)) {
			System.out.println("Rezerwacja została dodana klient: " + reservation.getClient().getFirstName() + " "
					+ reservation.getClient().getLastName());
			new Email().send(this, reservation);
		}
	}

	public void updateReservation(Reservation reservation) {
		reservations.set(reservations.indexOf(reservation), reservation);
		if (new DAO().updateClient(this)) {
			System.out.println("Rezerwacja została zmodyfikowana klient: " + reservation.getClient().getFirstName()
					+ " " + reservation.getClient().getLastName());
			new Email().send(this, reservation);
		}
	}

	public void removeReservation(Reservation reservation) {
		reservation.setReservationType(ReservationType.anulowana);
		reservations.remove(reservation);
		if (new DAO().updateClient(this)) {
			System.out.println("Rezerwacja została usunięta klient: " + reservation.getClient().getFirstName() + " "
					+ reservation.getClient().getLastName());
			new Email().send(this, reservation);
		}
	}

	@Override
	public String toString() {
		return "Client [clientid=" + clientID + ", firstName=" + firstName + ", lastName=" + lastName + ", phoneNumber="
				+ phoneNumber + ", email=" + email + "]";
	}
}