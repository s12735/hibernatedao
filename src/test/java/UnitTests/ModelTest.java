package UnitTests;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import Model.Reservation;
import Model.Client;

public class ModelTest {
	
	String firstName = "Piotr";
	String lastName = "Kowalski";
	String phoneNumber = "506436134";
	String email = "piotr.kowalski@wp.pl";
	
	@Test
	public void testUserClassConstructor(){
		Client user = new Client(firstName, lastName, phoneNumber,email, null);
		
		Assert.assertEquals(firstName, user.getFirstName());
		Assert.assertEquals(lastName, user.getLastName());
		Assert.assertEquals(phoneNumber, user.getPhoneNumber());
		Assert.assertEquals(email, user.getEmail());
	}
	
	@Test
	public void testUserClassSettersAndGetters(){
		Client user = new Client();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setPhoneNumber(phoneNumber);
		user.setEmail(email);
		
		Assert.assertEquals(firstName, user.getFirstName());
		Assert.assertEquals(lastName, user.getLastName());
		Assert.assertEquals(phoneNumber, user.getPhoneNumber());
		Assert.assertEquals(email, user.getEmail());	
		
		Assert.assertNotSame(firstName, user.getLastName());
	}
	
	@Test
	public void testReservationClassSettersAndGetters(){
		Date startDate = new Date();
		Date endDate = new Date(startDate.getTime() + (1000 * 60 * 60 * 24));
		
		Reservation reservation = new Reservation();
		
		reservation.setStartDate(startDate);
		reservation.setEndDate(endDate);
		
		Assert.assertEquals(startDate, reservation.getStartDate());
		Assert.assertEquals(endDate, reservation.getEndDate());
		
	}

}
